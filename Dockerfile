FROM node:9.5.0
EXPOSE 3000
ADD . /app
WORKDIR /app
CMD ["node", "server.js"]
USER node
