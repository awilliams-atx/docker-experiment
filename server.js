const http = require('http')
const server = http.createServer()
const PORT = 3000

server.on('request', (req, res) => {
  res.end('[NODEJS]')
})

server.listen(PORT, () => {
  console.log(`[NODEJS] Listening on ${PORT}`)
})


