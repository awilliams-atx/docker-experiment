# Docker Experiment

## Node.js source

```javascript
// server.js

const http = require('http')
const server = http.createServer()
const PORT = 3000

server.on('request', (req, res) => {
  res.end('[CONTAINERIZED NODEJS]')
})

server.listen(PORT, () => {
  console.log(`[NODEJS] Listening on ${PORT}`)
})
```

## Dockerfile
```
FROM node:9.5.0
EXPOSE 3000
ADD . /app
WORKDIR /app
CMD ["node", "server.js"]
USER node
```

* Build image from node version 9.5.0
* Expose container port 3000 to host
* Copy current directory contents to container directory /app
* cd container into /app
* When container is ready, run `node server.js`
* Set user to node instead of root

## Build image
```
docker build -t nodejs-server .
```
* Name image "nodejs-server"
* Use files in directory
## Start container/run server
```
docker run -d -p 3000:3000 --rm --name nodejs-erver nodejs-server
```
* Detach from container (to avoid shell session hijack)
* Route host port 3000 to container port 3000
* Remove container when it exits
* Name container "nodejs-server"
* Build from image "nodejs-server"
## Stop container/Kill server
```
docker container ls
```
```
CONTAINER ID        IMAGE               COMMAND             CREATED                  STATUS              PORTS                    NAMES
3203d141d5f0        nodejs-server       "node server.js"    Less than a second ago   Up 6 seconds        0.0.0.0:3000->3000/tcp   nodejs-server
```
```
docker kill 3203d151
```
## Delete image
```
docker image ls
```
```
REPOSITORY          TAG                 IMAGE ID            CREATED             SIZE
nodejs-server       latest              caff304773cf        2 minutes ago       676MB
node                9.5.0               39337023f8d4        3 days ago          676MB
node                latest              39337023f8d4        3 days ago          676MB
```
```
docker image rm caff304
```
